import ReactDOM from 'react-dom/client';
import App from './App';
import axios from "axios";
import { Provider } from 'react-redux';
import { store } from './store';



const token = localStorage.getItem('token');
axios.defaults.headers.common['Authorization'] =`Bearer ${token}`;

export default axios.create({
  baseURL:'https://news-portal321321312.herokuapp.com/api'
  
  // baseURL:'http://localhost:3000/api',
})


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Provider store={store}>
    <App/>
  </Provider>
);


