import { FormEvent, FormEventHandler, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "../../index"
const AddPost = () => {

  const [post, setPost] = useState({ title: '', content: '' });
  const navigate = useNavigate();
  async function fetchForm(e: FormEvent){
    e.preventDefault();
    await axios.post('/post',post);
    navigate('/');  
  }

  return (
    <form className="addpost__form" action="" onSubmit={fetchForm}>
      <label htmlFor="title">Заголовок</label>
      <input
        id="title"
        type="text"
        placeholder="Заголовок"
        value={post.title}
        onChange={e => setPost({ ...post, title: e.target.value })} />


      <label htmlFor="body">Текст</label>
      <textarea
        name=""
        id="body"
        value={post.content}
        onChange={e => setPost({ ...post, content: e.target.value })}
      ></textarea>

      <button>Создать новость</button>
    </form>
  );
};

export default AddPost;