import { FormEvent, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import PostService from '../../service/PostService';



function PostEdit() {
    const postServie = new PostService();
    const [post, setPost] = useState({ title: '', content: '' });
    const navigate = useNavigate();
    const {id} = useParams();
    useEffect(()=>{
        const getPost = async ()=>{
            const data = await postServie.getPost(id);
            console.log(data?.data);
            const post = data?.data; 
            setPost({title:post.title,content:post.content});
        }   
        getPost(); 
    },[]);


    async function fetchForm(e:FormEvent){
        e.preventDefault();
        await postServie.update(Number(id),post);
        navigate('/');
    }

    return (
        <form className="addpost__form" action="" onSubmit={fetchForm}>
        <label htmlFor="title">Заголовок</label>
        <input
          id="title"
          type="text"
          placeholder="Заголовок"
          value={post.title}
          onChange={e => setPost({ ...post, title: e.target.value })} />
  
  
        <label htmlFor="body">Текст</label>
        <textarea
          name=""
          id="body"
          value={post.content}
          onChange={e => setPost({ ...post, content: e.target.value })}
        ></textarea>
  
        <button>Сохранить</button>
      </form>
    );
  };
  
  
  export default PostEdit;